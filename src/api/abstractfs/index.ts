/**
 * /importer/v1/iiif_importer namespace
 * @see https://r1.bharatikosha.org/api/importer/v1/docs
 */

import { IContext } from '../../context';
import { APIEndpoint, IParams } from '../lib';
import { IAbstractFileGetParams, IAbstractFileFriendlyGetParams, IAbstractFsInfoGetParams } from './param-models';


export const namespaceBase = '/abstractfs/v1';


/**
 * gets an abstract file
 */
export const AbstractFile = new APIEndpoint<
    IAbstractFileGetParams, Blob,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'AbstractFile',
    urlPartParams: ['identifier'],
    url: (vc: IContext, { identifier }: IAbstractFileGetParams): string => `${vc.base}${namespaceBase}/${identifier}`,
    methods: {
        get: {},
    },
});


export const AbstractFileFriendly = new APIEndpoint<
    IAbstractFileFriendlyGetParams, Blob,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'AbstractFile',
    urlPartParams: ['source', 'fs_name', 'arg_str'],
    url: (vc: IContext, { source, fs_name, arg_str }: IAbstractFileFriendlyGetParams): string => `${vc.base}${namespaceBase}/${source}/${fs_name}/${arg_str}`,
    methods: {
        get: {},
    },
});


export const AbstractFsInfo = new APIEndpoint<
    IAbstractFsInfoGetParams, IParams,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'AbstractFile',
    urlPartParams: ['source', 'fs_name'],
    url: (vc: IContext, { source, fs_name }: IAbstractFsInfoGetParams): string => `${vc.base}${namespaceBase}/${source}/${fs_name}`,
    methods: {
        get: {},
    },
});
