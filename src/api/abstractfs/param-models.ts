import { IParams } from '../lib';

export interface IAbstractFileGetParams extends IParams {
    identifier: string;
}


export interface IAbstractIdentifierParts extends IParams {
    source: string;
    fs_name: string;
}


export interface IAbstractFsInfoGetParams extends IAbstractIdentifierParts {
    opts?: IParams,
}


export interface IAbstractFileFriendlyGetParams extends IAbstractIdentifierParts {
    arg_str: string;
}
