/**
 * /accounts/v1/authorization namespace
 * @see https://r1.bharatikosha.org/api/accounts/v1/docs
 */

import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { IAuthorizerGetParams, ISignInData, ITokenPostData } from './param-models';
import { IToken } from './response-models';

export const namespaceBase = '/accounts/v1/oauth';
export const makeURLFn = urlMakerFnFactory(namespaceBase);

/**
 * OAuth Authorization Request endpoint.
 * @see https://www.oauth.com/oauth2-servers/authorization/the-authorization-request/
 * Use endpoint objext for creating final authorization endpoint url, and redirect iuser to it;
 */
export const Authorizer = new APIEndpoint<
    IAuthorizerGetParams, any,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'accounts.auth.Authorizer',
    url: makeURLFn('/authorize'),
    methods: {
        get: {

        },
    },
});


/**
 * OAuth Token endpoint. for all types of tokens. attach appropriate params for specific flow/token types
 * @see https://www.oauth.com/oauth2-servers/access-tokens/
 */
export const Token = new APIEndpoint<
    any, any,
    any, ITokenPostData, IToken,
    any, any, any,
    any, any, any
>({
    name: 'accounts.auth.Token',
    url: makeURLFn('/token'),
    methods: {
        post: {

        },
    },
});


/**
 * primary sign In endpoint. useful on server side and tests.
 * web clients CAN NOT and SHOULD NOT use this endpoint, as they should be from same origin as api for that.
 */
export const SignIn = new APIEndpoint<
    any, any,
    any, ISignInData, any,
    any, any, any,
    any, any, any
>({
    name: 'accounts.auth.SignIn',
    url: makeURLFn('/signin'),
    methods: {
        post: {

        },
    },
});


/**
 * primary signout endpoint. useful on server side and tests.
 * web clients CAN NOT and SHOULD NOT use this endpoint, as they should be from same origin as api for that.
 */
export const SignOut = new APIEndpoint<
    any, any,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'accounts.auth.SignOut',
    url: makeURLFn('/signout'),
    methods: {
        get: {

        },
    },
});


export default {
    namespaceBase,
    Authorizer,
    Token,
    SignIn,
    SignOut,
};
