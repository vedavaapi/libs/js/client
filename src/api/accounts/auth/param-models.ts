import { IParams } from '../../lib';

/**
 * OAuth authorization request init params
 * @see https://www.oauth.com/oauth2-servers/authorization/the-authorization-request/
 */
export interface IAuthorizerGetParams extends IParams {
    /** Vedavaapi OAuth Client Id */
    client_id: string;
    response_type: 'code' | 'token';
    scope: string;
    redirect_uri: string;
    state: string;
}

/**
 * OAuth token request data
 * @see https://www.oauth.com/oauth2-servers/access-tokens/
 */
export interface ITokenPostData extends IParams {
    client_id?: string;
    client_secret?: string;
    code?: string;
    grant_type?: 'authorization_code' | 'refresh_token' | 'implicit' | 'client_credentials' | 'password';
    redirect_uri?: string;
    refresh_token?: string;
}


/**
 * sign in request data
 */
export interface ISignInData {
    email: string;
    password: string;
}
