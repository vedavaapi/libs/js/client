
/**
 * access token response
 * @see https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/
 */
export interface IToken {
    access_token: string;
    token_type: 'Bearer' | string;
    expires_in: number;
    refresh_token?: string;
    scope?: string;
}
