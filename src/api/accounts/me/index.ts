/**
 * /accounts/v1/me namespace
 * @see https://r1.bharatikosha.org/api/accounts/v1/docs
 */
import { getMarshalMiddleware } from '../../../context';
import { ITeam, IUser } from '../resource-models';
import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { IMeGetParams, IMePostData, IMeTeamsGetParams } from './param-models';

export const namespaceBase = '/accounts/v1/me';
const makeURLFn = urlMakerFnFactory(namespaceBase);


/**
 * get/post user details whom current access token representing
 */
export const Me = new APIEndpoint<
    IMeGetParams, IUser,
    any, IMePostData, IUser,
    any, any, any,
    any, any, any
>({
    name: 'accounts.me.Me',
    url: makeURLFn(''),
    methods: {
        get: {
            middleware: getMarshalMiddleware<IUser, IUser>(),
        },
        post: {
            middleware: getMarshalMiddleware<IUser, IUser>(),
        },
    },
});

/**
 * get all teams in which current user(represented by accessToken) is effectively member of
 */
export const MyTeams = new APIEndpoint<
    IMeTeamsGetParams, ITeam[],
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'accounts.me.MyTeams',
    url: makeURLFn('/teams'),
    methods: {
        get: {
            middleware: getMarshalMiddleware<ITeam[], ITeam[]>(),
        },
    },
});

export default {
    namespaceBase,
    Me,
    MyTeams,
};
