import { IProjection } from '../../api-param-models';
import { IParams } from '../../lib';
import { IUser } from '../resource-models';

export interface IMeGetParams extends IParams {
    projection?: IProjection;
}

export interface IMePostData extends IParams {
    update_doc: IUser;
    return_projection?: IProjection;
}

export interface IMeTeamsGetParams extends IParams {
    teams_projection?: IProjection;
    /** @deprecated */
    return_count?: boolean;
}
