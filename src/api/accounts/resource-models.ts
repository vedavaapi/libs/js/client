import { IResource } from '../../context';


export interface IUser extends IResource {
    name?: string;
    email: string;
    password?: string;
}


export interface ITeam extends IResource {
    name: string;
    description?: string;
    members?: string[];
}
