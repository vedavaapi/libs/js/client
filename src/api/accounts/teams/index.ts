/**
 * /accounts/v1/teams namespace
 * @see https://r1.bharatikosha.org/api/accounts/v1/docs
 */

import { IContext, getMarshalMiddleware } from '../../../context';
import { ITeam } from '../resource-models';
import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { IResourcesDeleteResponseOld, IResourcesResponse, ResourcesResponse } from '../../objstore/response-models';
import { ITeamGetParams, ITeamIdentifierParams, ITeamMembersGetParams, ITeamMembersUpdateData, ITeamPostData, ITeamsDeleteData, ITeamsGetParams, ITeamsPostData } from './param-models';
import { ITeamMembers } from './response-models';


export const namespaceBase = '/accounts/v1/teams';
const makeURLFn = urlMakerFnFactory(namespaceBase);

/**
 * get teams; post a team; delete teams with known _ids
 */
export const Teams = new APIEndpoint<
    ITeamsGetParams, ResourcesResponse<ITeam>,
    any, ITeamsPostData, ITeam,
    any, any, any,
    any, ITeamsDeleteData, IResourcesDeleteResponseOld
>({
    name: 'accounts.teams.Teams',
    url: makeURLFn(''),
    methods: {
        get: {
            middleware: ResourcesResponse.getMarshalMiddleware<IResourcesResponse, ITeam>(),
        },
        post: {
            middleware: getMarshalMiddleware<ITeam, ITeam>(),
        },
        delete: {

        },
    },
});

/**
 * get/update team Resource; accepts both _id and name as identifiers for team
 */
export const Team = new APIEndpoint<
    ITeamGetParams, ITeam,
    ITeamIdentifierParams, ITeamPostData, ITeam,
    any, any, any,
    any, any, any
>({
    name: 'accounts.teams.Team',
    urlPartParams: ['team_identifier'],
    url: (vc: IContext, { team_identifier }: ITeamGetParams): string => `${vc.base}${namespaceBase}/${team_identifier}`,
    methods: {
        get: {
            middleware: getMarshalMiddleware<ITeam, ITeam>(),
        },
        post: {
            middleware: getMarshalMiddleware<ITeam, ITeam>(),
        },
    },
});

/**
 * get teams resolved/unresolved members; add/delete expilicit members to team
 */
export const TeamMembers = new APIEndpoint<
    ITeamMembersGetParams, ITeamMembers,
    ITeamIdentifierParams, ITeamMembersUpdateData, ITeamMembers,
    any, any, any,
    ITeamIdentifierParams, ITeamMembersUpdateData, ITeamMembers
>({
    name: 'accounts.teams.Members',
    urlPartParams: ['team_identifier'],
    url: (vc: IContext, { team_identifier }: ITeamIdentifierParams): string => `${vc.base}${namespaceBase}/${team_identifier}/members`,
    methods: {
        get: {

        },
        post: {

        },
        delete: {

        },
    },
});

export default {
    namespaceBase,
    Teams,
    Team,
    TeamMembers,
};
