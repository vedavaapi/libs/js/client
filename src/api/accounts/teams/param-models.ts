import { ITeam } from '../resource-models';
import { IProjection, IResourcesGetCommonParams } from '../../api-param-models';
import { IParams } from '../../lib';

export interface ITeamsGetParams extends IResourcesGetCommonParams {
    selector_doc?: any;
}


export interface ITeamsPostData extends IParams {
    team_json: ITeam;
    return_projection?: IProjection
}


export interface ITeamsDeleteData extends IParams {
    team_ids: string[];
}

export interface ITeamIdentifierParams extends IParams {
    team_identifier: string;
    identifier_type?: '_id' | 'name';
}

export interface ITeamGetParams extends ITeamIdentifierParams {
    projection?: IProjection;
}

export interface ITeamPostData extends IParams {
    update_doc: ITeam;
    return_projection?: IProjection;
}

export interface ITeamMembersGetParams extends ITeamIdentifierParams {
    only_explicit_members?: boolean;
}

export interface ITeamMembersUpdateData extends IParams {
    member_identifiers: string[];
    member_identifiers_type: '_id' | 'email';
}
