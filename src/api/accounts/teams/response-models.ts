
/**
 * array of team member ids
 */
export interface ITeamMembers {
    [index: number]: string;
}
