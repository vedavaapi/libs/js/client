/**
 * /accounts/v1/users namespace
 * @see https://r1.bharatikosha.org/api/accounts/v1/docs
 */

import { IContext, getMarshalMiddleware } from '../../../context';
import { ITeam, IUser } from '../resource-models';
import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { IResourcesDeleteResponseOld, IResourcesResponse, ResourcesResponse } from '../../objstore/response-models';
import { IUserGetParams, IUserPostData, IUsersDeleteData, IUsersGetParams, IUsersPostData, IUserTeamsGetParams } from './param-models';


export const namespaceBase = '/accounts/v1/users';
const makeURLFn = urlMakerFnFactory(namespaceBase);


/**
 * get matched users, create new user, delete users
 */
export const Users = new APIEndpoint<
    IUsersGetParams, ResourcesResponse<IUser>,
    any, IUsersPostData, IUser,
    any, any, any,
    any, IUsersDeleteData, IResourcesDeleteResponseOld
>({
    name: 'accounts.users.Users',
    url: makeURLFn(''),
    methods: {
        get: {
            middleware: ResourcesResponse.getMarshalMiddleware<IResourcesResponse, IUser>(),
        },
        post: {
            middleware: getMarshalMiddleware<IUser, IUser>(),
        },
        delete: {

        },
    },
});

/**
 * get or update existing info of user with given id
 */
export const User = new APIEndpoint<
    IUserGetParams, IUser,
    { user_id: string }, IUserPostData, IUser,
    any, any, any,
    any, any, any
>({
    name: 'accounts.users.User',
    urlPartParams: ['user_id'],
    url: (vc: IContext, { user_id }: IUserGetParams): string => `${vc.base}${namespaceBase}/${user_id}`,
    methods: {
        get: {
            middleware: getMarshalMiddleware<IUser, IUser>(),
        },
        post: {
            middleware: getMarshalMiddleware<IUser, IUser>(),
        },
    },
});

/**
 * get teams in which given user is effectively a member
 */
export const UserTeams = new APIEndpoint<
    IUserTeamsGetParams, ITeam[],
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'accounts.users.UserTeams',
    urlPartParams: ['user_id'],
    url: (vc: IContext, { user_id }: IUserTeamsGetParams): string => `${vc.base}${namespaceBase}/${user_id}/teams`,
    methods: {
        get: {
            middleware: getMarshalMiddleware<ITeam[], ITeam[]>(),
        },
    },
});

export default {
    namespaceBase,
    Users,
    User,
    UserTeams,
};
