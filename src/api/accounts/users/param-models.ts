import { IUser } from '../resource-models';
import { IProjection, IResourcesGetCommonParams } from '../../api-param-models';
import { IParams } from '../../lib';

export interface IUsersGetParams extends IResourcesGetCommonParams {
    selector_doc?: any;
}


export interface IUsersPostData extends IParams {
    user_json: IUser;
    initial_team_id?: string;
    return_projection?: IProjection
}


export interface IUsersDeleteData extends IParams {
    user_ids: string[];
}


export interface IUserGetParams extends IParams {
    user_id: string;
    projection?: IProjection;
}

export interface IUserPostData extends IParams {
    update_doc: IUser;
    return_projection?: IProjection;
}

export interface IUserTeamsGetParams extends IParams {
    user_id: string;
    teams_projection?: IProjection;
}
