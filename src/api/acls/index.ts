/**
 * /acls/v1 api
 * @see https://r1.bharatikosha.org/api/acls/v1/docs
 */

import { IContext, IResource, getMarshalMiddleware } from '../../context';
import { APIEndpoint, IURLGen } from '../lib';
import { IAclsResourceIdParams, IAclsUpdateParams } from './param-models';
import { IResolvedPermissions } from './response-models';

export const namespaceBase = '/acls/v1';

const urlGenFn: IURLGen = (vc: IContext, { resource_id }: IAclsResourceIdParams): string => `${vc.base}${namespaceBase}/${resource_id}`;

const marshalMiddleware = getMarshalMiddleware<IResource, IResource>();

/**
 * endpoint to change acls of resource.
 */
export const Acls = new APIEndpoint<
    IAclsResourceIdParams, IResource,
    IAclsResourceIdParams, IAclsUpdateParams, IResource,
    any, any, any,
    IAclsResourceIdParams, IAclsUpdateParams, IResource
>({
    name: 'acls.Acls',
    urlPartParams: ['resource_id'],
    url: urlGenFn,
    methods: {
        get: {
            middleware: marshalMiddleware,
        },
        post: {
            middleware: marshalMiddleware,
        },
        delete: {
            middleware: marshalMiddleware,
        },
    },
});

/**
 * retrieve resolvedPermissions over an object for current user
 */
export const ResolvedPermissions = new APIEndpoint<
    IAclsResourceIdParams, IResolvedPermissions,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'acls.ResolvedPermissions',
    urlPartParams: ['resource_id'],
    url: (vc: IContext, { resource_id }: IAclsResourceIdParams): string => `${vc.base}${namespaceBase}/${resource_id}/resolved_permissions`,
    methods: {
        get: {

        },
    },
});
