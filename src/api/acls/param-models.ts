import { IParams } from '../lib';

export interface IAclsResourceIdParams extends IParams {
    resource_id: string;
}


export type IAclAction = 'read' | 'updateContent' | 'updateLinks' | 'updatePermissions' | 'delete' | 'createChildren' | 'createAnnos';


export interface IAclsUpdateParams extends IParams {
    actions: IAclAction[];
    control: 'grant' | 'revoke' | 'block';
    user_ids?: string[];
    team_ids: string[];
}
