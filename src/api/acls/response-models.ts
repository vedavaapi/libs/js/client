
export interface IResolvedPermissions {
    jsonClass: 'ResolvedPermissions';

    read: boolean;

    updateContent: boolean;

    updateLinks: boolean;

    updatePermissions: boolean;

    createAnnos: boolean;

    createChildren: boolean;

    delete : boolean;
}
