import { IResource } from '../context';
import { IParams } from './lib';

export interface IProjection {
    [field: string]: 0 | 1;
}

export interface IIDResourceMap {
    [id: string]: IResource;
}

export interface IResourcesGetCommonParams extends IParams {
    projection?: IProjection;
    sort_doc?: Array<[string, 1 | -1]>;
    start?: number;
    count?: number;
    attach_in_links?: boolean;
}

export interface ITaskIdParams extends IParams {
    task_id: string;
}
