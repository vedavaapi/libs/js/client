/**
 * /iiif_image/v1 api
 * provides iiif image api 2.1 @see https://iiif.io/api/image/2.1/;
 * it serves projection of image resources in objstore api in iiif 2.1 spec compilent way
 * @see https://r1.bharatikosha.org/api/iiif_image/v1/docs
 */

import { IContext } from '../../context';
import { APIEndpoint } from '../lib';
import { IIIFIdentParams, IIIFImageParams } from './param-models';
import { IIIFImageInfo } from './response-models';


export const namespaceBase = '/iiif_image/v1';

/**
 *  iiif image info.json
 * @see https://iiif.io/api/image/2.1/#image-information
 */
export const Info = new APIEndpoint<
    IIIFIdentParams, IIIFImageInfo,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiif_image.Info',
    urlPartParams: ['service', 'ident'],
    url: (vc: IContext, { service, ident }: IIIFIdentParams): string => `${vc.base}${namespaceBase}/${service}/${ident}/info.json`,
    methods: {
        get: {

        },
    },
});

/**
 * @example - https://r1.bharatikosha.org/api/iiif_image/v1/objstore/5d5efcb7cd48a30010e80334/info.json
 */
Info.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/([^/]+)/info.json$`);

/**
 * returns iiif image info url components
 */
Info.urlComponents = (url: string) => {
    const matchResult = url.match(Info.urlRegex);
    if (!matchResult) {
        return null;
    }
    const [, apiBase, service, ident] = matchResult;
    return { apiBase, service, ident };
};


/**
 * iiif image endpoint. use @method: url() for getting resolved url
 * @see https://iiif.io/api/image/2.1/#image-request-parameters
 */
export const Image = new APIEndpoint<
    IIIFImageParams, Blob,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiif_image.Image',
    urlPartParams: ['service', 'ident', 'region', 'size', 'rotation', 'quality', 'format'],
    url: (vc: IContext, { service, ident, region, size, rotation, quality, format }: IIIFImageParams): string => `${vc.base}${namespaceBase}/${service}/${ident}/${region}/${size}/${rotation}/${quality}.${format}`,
    methods: {
        get: {

        },
    },
});


/**
 * @example: https://r1.bharatikosha.org/api/iiif_image/v1/objstore/5d5efcb7cd48a30010e80334/full/full/0/default.jpg
 */
// eslint-disable-next-line no-useless-escape
Image.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/([^/]+)/([^/]+)/([^/]+)/([^/]+)/([^/]+)\\\.([^/]+)$`);

/**
 * returns iiif image url components
 */
Image.urlComponents = (url: string) => {
    const matchResult = url.match(Image.urlRegex);
    if (!matchResult) {
        return null;
    }
    const [, apiBase, service, ident, region, size, rotation, quality, format] = matchResult;
    return {
        apiBase,
        service,
        ident,
        region,
        size,
        rotation,
        quality,
        format,
    };
};
