
export interface IIIFIdentParams {
    service: string;
    ident: string;
}

/**
 * @see https://iiif.io/api/image/2.1/#image-request-parameters
 */
export interface IIIFImageParams extends IIIFIdentParams {
    /** full | square | x,y,w,h | pct:x,y,w,h */
    region: string;
    /** full | max | w,h | w, | ,h | pct:n | !w,h */
    size: string;
    /** n | !n */
    rotation: string;
    quality: 'color' | 'gray' | 'bitonal' | 'default';
    format: 'jpg' | 'png' | 'tif' | 'gif' | 'webp' | 'jp2';
}
