/**
 * @see https://iiif.io/api/image/2.1/#image-information
 */
export interface IIIFImageInfo {
    // NOTE
    '@context'?: string;
    '@id'?: string;
    height: number;
    width: number;
    [key: string]: any;
}
