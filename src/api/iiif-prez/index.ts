/**
 * /iiif_presentation/v1 api;
 * iiif presentation api 2.1; @see https://r1.bharatikosha.org/api/iiif_presentation/v1/docs
 * it servers projection of objstore resources in iiif-presentation 2.1 compilent way, according to their iiif_models;
 * currently we can address all Manifest, Sequence, Canvas, Collections uniquely;
 * supports iiif presentation api 2.1; @see https://iiif.io/api/presentation/2.1/
 */

import { IContext } from '../../context';
import { APIEndpoint } from '../lib';
import { IIIFCanvasParams, IIIFCollectionParams, IIIFResourceCommonURLParts, IIIFSequenceParams } from './param-models';
import { IIIFCanvas, IIIFCollection, IIIFManifest, IIIFSequence } from './response-models';


export const namespaceBase = '/iiif_presentation/v1';

/**
 * iiif manifest endpoint;
 * @see https://iiif.io/api/presentation/2.1/#manifest
 */
export const Manifest = new APIEndpoint<
    IIIFResourceCommonURLParts, IIIFManifest,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiifp.Manifest',
    urlPartParams: ['service', 'object_id'],
    url: (vc: IContext, { service, object_id }: IIIFResourceCommonURLParts): string => `${vc.base}${namespaceBase}/${service}/${object_id}/manifest.json`,
    methods: {
        get: {

        },
    },
});


Manifest.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/([^/]+)/manifest\\\.json$`);

/**
 * returns manifest url components
 */
Manifest.urlComponents = (url: string) => {
    const matchResult = url.match(Manifest.urlRegex);
    if (!matchResult) {
        return null;
    }

    // tslint:disable-next-line: variable-name
    const [, apiBase, service, object_id] = matchResult;
    return { apiBase, service, object_id };
};

/**
 * iiif sequence endpoint
 * @see https://iiif.io/api/presentation/2.1/#sequence
 */
export const Sequence = new APIEndpoint<
    IIIFSequenceParams, IIIFSequence,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiifp.Sequence',
    urlPartParams: ['service', 'object_id', 'sequence_id'],
    url: (vc: IContext, { service, object_id, sequence_id }: IIIFSequenceParams): string => `${vc.base}${namespaceBase}/${service}/${object_id}/sequence/${sequence_id}.json`,
    methods: {
        get: {

        },
    },
});

Sequence.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/([^/]+)/sequence/([^/]+)\\\.json$`);

/**
 * returns sequence url components
 */
Sequence.urlComponents = (url: string) => {
    const matchResult = url.match(Sequence.urlRegex);
    if (!matchResult) {
        return null;
    }

    // tslint:disable-next-line: variable-name
    const [, apiBase, service, object_id, sequence_id] = matchResult;
    return { apiBase, service, object_id, sequence_id };
};

/**
 * iiif canvas endpoint
 * @see https://iiif.io/api/presentation/2.1/#canvas
 */
export const Canvas = new APIEndpoint<
    IIIFCanvasParams, IIIFCanvas,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiifp.Canvas',
    urlPartParams: ['service', 'object_id', 'canvas_id'],
    url: (vc: IContext, { service, object_id, canvas_id }: IIIFCanvasParams): string => `${vc.base}${namespaceBase}/${service}/${object_id}/canvas/${canvas_id}.json`,
    methods: {
        get: {

        },
    },
});

// eslint-disable-next-line no-useless-escape
Canvas.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/([^/]+)/canvas/([^/]+)\\\.json$`);

/**
 * returns canvas uri components
 */
Canvas.urlComponents = (url: string) => {
    const matchResult = url.match(Canvas.urlRegex);
    if (!matchResult) {
        return null;
    }

    // tslint:disable-next-line: variable-name
    const [, apiBase, service, object_id, canvas_id] = matchResult;
    return { apiBase, service, object_id, canvas_id };
};

/**
 * iiif collection endpoint
 * @see https://iiif.io/api/presentation/2.1/#collection
 */
export const Collection = new APIEndpoint<
    IIIFCollectionParams, IIIFCollection,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'iiifp.Collection',
    urlPartParams: ['service', 'collection_id'],
    url: (vc: IContext, { service, collection_id }: IIIFCollectionParams): string => `${vc.base}${namespaceBase}/${service}/collection/${collection_id}.json`,
    methods: {
        get: {

        },
    },
});


// eslint-disable-next-line no-useless-escape
Collection.urlRegex = new RegExp(`(^.*)${namespaceBase}/([^/]+)/collection/([^/]+)\\\.json$`);

/**
 * returns collection url components
 */
Collection.urlComponents = (url: string) => {
    const matchResult = url.match(Collection.urlRegex);
    if (!matchResult) {
        return null;
    }

    // tslint:disable-next-line: variable-name
    const [, apiBase, service, collection_id] = matchResult;
    return { apiBase, service, collection_id };
};
