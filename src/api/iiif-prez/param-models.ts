import { IParams } from '../lib';

export interface IIIFCollectionParams extends IParams {
    service: string;
    collection_id: string;
}


export interface IIIFResourceCommonURLParts extends IParams {
    service: string;
    object_id: string;
}

export interface IIIFSequenceParams extends IIIFResourceCommonURLParts {
    sequence_id: string;
}

export interface IIIFCanvasParams extends IIIFResourceCommonURLParts {
    canvas_id: string;
}
