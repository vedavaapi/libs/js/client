import { IIIFImageInfo } from '../iii-image/response-models';

export interface IIIFResourceCommonAttrs {
    '@context': string;
    '@type': string;
    '@id': string;
    label?: string;
    metadata?: {
        [n: number]: {
            label: string;
            value: any;
        }
    };
    description?: string;
    thumbnail?: string | IIIFImageInfo
}


/**
 * @see https://iiif.io/api/presentation/2.1/#manifest
 */
export interface IIIFManifest extends IIIFResourceCommonAttrs {
    label: string;
    sequences: IIIFSequence[];
}


/**
 * @see https://iiif.io/api/presentation/2.1/#sequence
 */
export interface IIIFSequence extends IIIFResourceCommonAttrs {
    canvases: IIIFCanvas[];
}


/**
 * @see https://iiif.io/api/presentation/2.1/#canvas
 */
export interface IIIFCanvas extends IIIFResourceCommonAttrs {
    label: string;
    height: number;
    width: number;
}

/**
 * @see https://iiif.io/api/presentation/2.1/#collection
 */
export interface IIIFCollection extends IIIFResourceCommonAttrs {
    label: string;
    collections?: IIIFCollection[];
    manifests?: IIIFManifest[];
}
