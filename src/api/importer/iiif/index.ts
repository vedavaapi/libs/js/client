/**
 * /importer/v1/iiif_importer namespace
 * @see https://r1.bharatikosha.org/api/importer/v1/docs
 */

import { IContext } from '../../../context';
import { ITaskIdParams } from '../../api-param-models';
import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { ITaskDeleteStatus as ITaskDeleteResponse, ITaskResponse, ITaskStatus } from '../../response-models';
import { IIIFImportTasksPostData } from './param-models';


export const namespaceBase = '/importer/v1/iiif_importer';
const makeURLFn = urlMakerFnFactory(namespaceBase);

/**
 * iiif book import task
 * takes manifest url or namespace specific urls, and returns task descriptor
 */
export const ImportTasks = new APIEndpoint<
    any, any,
    any, IIIFImportTasksPostData, ITaskResponse,
    any, any, any,
    any, any, any
>({
    name: 'importer.iiif.ImportTasks',
    url: makeURLFn('/import_task'),
    methods: {
        post: {

        },
    },
});

/**
 * async task status/delete endpint
 */
export const Task = new APIEndpoint<
    ITaskIdParams, ITaskStatus,
    any, any, any,
    any, any, any,
    ITaskIdParams, any, ITaskDeleteResponse
>({
    name: 'importer.iiif.Task',
    urlPartParams: ['task_id'],
    url: (vc: IContext, { task_id }: ITaskIdParams): string => `${vc.base}${namespaceBase}/tasks/${task_id}`,
    methods: {
        get: {

        },
        delete: {

        },
    },
});
