import { IParams } from '../../lib';

export interface IIIFImportTasksPostData extends IParams {
    library: string;
    url: string;
    upsert?: boolean;
}
