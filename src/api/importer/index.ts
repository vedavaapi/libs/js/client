/**
 * /importer/v1 api endpoint
 * @see https://r1.bharatikosha.org/api/importer/v1/docs
 */

import * as iiif from './iiif';
import * as pdf from './pdf';

export { iiif };
export { pdf };
