/**
 * /importer/v1/iiif_importer namespace
 * @see https://r1.bharatikosha.org/api/importer/v1/docs
 */

import { IContext } from '../../../context';
import { ITaskIdParams } from '../../api-param-models';
import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { ITaskDeleteStatus as ITaskDeleteResponse, ITaskResponse, ITaskStatus } from '../../response-models';
import { IPdfImportData, IPdfMultiImportTasksData } from './param-models';
import { IPdfImportResponse } from './response-models';


export const namespaceBase = '/importer/v1/pdf_importer';
const makeURLFn = urlMakerFnFactory(namespaceBase);


/**
 * import a pdf to given book
 */
export const Importer = new APIEndpoint<
    any, any,
    any, IPdfImportData, IPdfImportResponse,
    any, any, any,
    any, any, any
>({
    name: 'PdfImporter',
    url: makeURLFn('/import'),
    methods: {
        post: {},
    },
});


/**
 * import multiple pdf items
 */
export const MultiImportTasks = new APIEndpoint<
    any, any,
    any, IPdfMultiImportTasksData, ITaskResponse,
    any, any, any,
    any, any, any
>({
    name: 'PdfImporter',
    url: makeURLFn('/multi_import_tasks'),
    methods: {
        post: {},
    },
});


/**
 * async task status/delete endpint
 */
export const Task = new APIEndpoint<
    ITaskIdParams, ITaskStatus,
    any, any, any,
    any, any, any,
    ITaskIdParams, any, ITaskDeleteResponse
>({
    name: 'importer.iiif.Task',
    urlPartParams: ['task_id'],
    url: (vc: IContext, { task_id }: ITaskIdParams): string => `${vc.base}${namespaceBase}/tasks/${task_id}`,
    methods: {
        get: {

        },
        delete: {

        },
    },
});
