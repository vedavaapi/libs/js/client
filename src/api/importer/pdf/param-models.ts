import { IParams } from '../../lib';


export interface IPdfImportData extends IParams {
    book_id: string;
    pdf_id?: string;
    pdffs_args?: string;
    is_scanned_pdf?: Boolean;
    set_cover?: boolean;
}


export interface IPdfMultiImportTasksData extends IParams {
    item_specs: Array<string|string[]>;
    pdffs_args?: string;
    are_scanned_pdfs?: Boolean;
    set_cover?: boolean;
}
