export interface IPdfImportResponse {
    book_id: string;
    page_count: number;
    seq_id: string;
}
