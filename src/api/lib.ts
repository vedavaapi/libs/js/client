// eslint-disable-next-line import/no-duplicates
import * as vweb from '@vedavaapi/web';
// eslint-disable-next-line import/no-duplicates
import { IVResponse } from '@vedavaapi/web';
import { getFormData, getHeaders, getSearchParams, IOpaqueDataInfo } from '../web-utils';

import { IContext, IModelsRegistry } from '../context';
import { APIClientError } from '../errors';

export interface IParams {
    [param: string]: any;
}


export interface IResponse<DT> extends IVResponse {
    data: DT;
}

export type APIMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

/**
 * custom Request init params interface; sameas fetch Request's init;
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Request/Request#Parameters;
 */
export interface IInit {
    headers?: {[header: string]: string};
    cache?: RequestCache;
    credentials?: RequestCredentials;
    integrity?: string;
    mode?: RequestMode;
    redirect?: RequestRedirect;
    referrer?: string;
    referrerPolicy?: ReferrerPolicy;
    [initArg: string]: any;
}


/**
 * static class providing interface for API HTTP methods. handles json stringification, formdata computation, search params computation, etc.
 */
export class APIRequest {
    public static async get(vc: IContext, url: string, params?: IParams, init: IInit = {}): Promise<IResponse<any>> {
        return APIRequest.request(vc, url, 'GET', undefined, params, init);
    }

    public static async post(vc: IContext, url: string, data?: IParams, odInfo?: IOpaqueDataInfo, params?: IParams, init: IInit = {}): Promise<IResponse<any>> {
        return APIRequest.request(vc, url, 'POST', data, params, init);
    }

    public static async delete(vc: IContext, url: string, data?: IParams, params?: IParams, init: IInit = {}): Promise<IResponse<any>> {
        return APIRequest.request(vc, url, 'DELETE', data, params, init);
    }

    public static async put(vc: IContext, url: string, data?: IParams, odInfo?: IOpaqueDataInfo, params?: IParams, init: IInit = {}): Promise<IResponse<any>> {
        return APIRequest.request(vc, url, 'PUT', data, params, init);
    }

    /**
     *
     * @param vc Context
     * @param url url
     * @param method GET | POST | PUT | DELETE
     * @param data dictionory of body params;
     * @param params dictionary of query params
     * @param init custom Request init overridings
     * @throws APIError
     */
    public static async request(
        vc: IContext,
        url: string,
        method: APIMethod,
        data?: IParams,
        odInfo?: IOpaqueDataInfo,
        params?: IParams,
        init: IInit = {},
    ): Promise<IResponse<any>> {
        const safeInit = init || {};
        let fullURL = url;
        if (params) {
            const searchParams = getSearchParams(params);
            if (searchParams !== '') {
                fullURL = `${url}?${searchParams}`;
            }
        }
        const fetchInit = {
            mode: 'cors',
            ...(safeInit),
            method,
            headers: getHeaders(vc, safeInit.headers),
        };
        if (method !== 'GET' && data) {
            const body = typeof (data) === 'string' ? data : getFormData(data, odInfo);
            // @ts-ignore
            fetchInit.body = body;
        }
        return vweb.request(fullURL, fetchInit as IInit);
    }
}


export type IURLGen = (vc: IContext, urlParts: any) => string;

export const urlMakerFnFactory = (namespaceBase: string) => (endpoint: string): IURLGen => (vc: IContext): string => `${vc.base}${namespaceBase}${endpoint}`;

export type IResponseMiddleware<T> = (data: any, mr?: IModelsRegistry) => T;

export interface IEndpointMethodConfig<R> {
    /** response middleware function for an endpoint method. */
    middleware?: IResponseMiddleware<R>;
    /** Opaque Data Info  */
    odInfo?: IOpaqueDataInfo;
}

/**
 * Configuration for an API Endpoint methods
 */
export interface IEndpointMethodsConfig<GR, PR, TR, DR> {
    get?: IEndpointMethodConfig<GR>;
    post?: IEndpointMethodConfig<PR>;
    put?: IEndpointMethodConfig<TR>;
    delete?: IEndpointMethodConfig<DR>;
}

/**
 * Configuration of an api endpoint instance
 */
export interface IEndpointConfig<GR, PR, TR, DR> {
    /** (better unique) name of endpoint. will be used in error stackTraces, if any. */
    name: string,
    /** params which are components of url path */
    urlPartParams?: string[];
    /** url generator function for an api endpoint */
    url: IURLGen;
    /** api methods configurations */
    methods: IEndpointMethodsConfig<GR, PR, TR, DR>;
}

// tslint:disable-next-line: interface-name
export interface APIRequestInit<P, D> {
    vc: IContext,
    params?: P;
    data?: D;
    init?: IInit;
    marshal?: boolean;
}

export interface APIRequestGenericInit<P, D> extends APIRequestInit<P, D> {
    method: APIMethod;
}

/**
 * APIEndpoint class; api endpoints are objects of this class, inheriting all it's prototype properties
 * have to supply appropriate endpoint configuration, to instantiate an endpoint object;
 * provides methods corresponding to HTTP methods get, post, put, delete
 * also provides method specific marshaller methods too, so that we can first request pure responses, and then marshal when required. needed in environments where POJOS are needed.
 */
export class APIEndpoint<GP extends IParams, GR, PP extends IParams, PD, PR, TP extends IParams, TD, TR, DP extends IParams, DD, DR> {
    [x: string]: any;

    private config: IEndpointConfig<GR, PR, TR, DR>;

    private endpointUrlFn: IURLGen;

    constructor(config: IEndpointConfig<GR, PR, TR, DR>) {
        this.config = config;
        this.endpointUrlFn = this.config.url;
    }

    /**
     * returns full url computed from params. if required params for a given endpoint are not present, raises APIClientError
     * @param vc - Context
     * @param params - params associated with endpont
     */
    public url({ vc, params }: {vc: IContext, params?: GP}): string {
        const urlParts = this.computeURLParts(this.config.urlPartParams, params);
        const finalParams = params ? { ...params } : undefined;
        if (urlParts && finalParams) {
            Object.keys(urlParts).forEach((k) => {
                delete finalParams[k];
            });
        }
        let fullURL = this.endpointUrlFn(vc, urlParts);
        if (finalParams) {
            const searchParams = getSearchParams(finalParams);
            if (searchParams !== '') {
                fullURL = `${fullURL}?${searchParams}`;
            }
        }
        return fullURL;
    }

    private getMethodConfig(method: APIMethod): IEndpointMethodConfig<any> {
        const iMethod = method.toLowerCase();
        // @ts-ignore
        const methodConfig = this.config.methods[iMethod] as IEndpointMethodConfig<any>;
        if (!methodConfig) {
            throw new APIClientError(`"${method}" not defined for "${this.config.name}" endpoint`);
        }
        return methodConfig;
    }

    public marshalResponse(method: APIMethod, response: IResponse<any>, mr?: IModelsRegistry): any {
        const methodConfig = this.getMethodConfig(method);
        if (!methodConfig || !methodConfig.middleware) {
            return response as IResponse<any>;
        }
        response.data = methodConfig.middleware(response.data, mr);
        return response as IResponse<any>;
    }

    public async request<MP extends IParams, MD extends IParams, MR>(
        { vc, method, params, data, init, marshal = true }: APIRequestGenericInit<MP, MD>,
    ): Promise<IResponse<MR>> {
        const methodConfig = this.getMethodConfig(method) as IEndpointMethodConfig<MR> | undefined;

        const urlParts = this.computeURLParts(this.config.urlPartParams, params);
        const finalParams = params ? { ...params } : undefined;
        if (urlParts && finalParams) {
            Object.keys(urlParts).forEach((k) => {
                delete finalParams[k];
            });
        }
        const url = this.endpointUrlFn(vc, urlParts);
        const odInfo = methodConfig ? methodConfig.odInfo : undefined;
        const response = await APIRequest.request(vc, url, method, data, odInfo, finalParams, init);

        if (marshal) {
            this.marshalResponse(method, response, vc.modelsRegistry);
        }
        return response as IResponse<MR>;
    }

    public async get({ vc, params, init, marshal = true }: APIRequestInit<GP, undefined>): Promise<IResponse<GR>> {
        return this.request<GP, IParams, GR>({ vc, method: 'GET', params, init, marshal });
    }

    public marshalGetResponse(response: IResponse<any>, mr?: IModelsRegistry): IResponse<GR> {
        return this.marshalResponse('GET', response, mr) as IResponse<GR>;
    }

    public async post({ vc, params, data, init, marshal = true }: APIRequestInit<PP, PD>): Promise<IResponse<PR>> {
        return this.request<PP, PD, PR>({ vc, method: 'POST', data, params, init, marshal });
    }

    public marshalPostResponse(response: IResponse<any>, mr?: IModelsRegistry): IResponse<PR> {
        return this.marshalResponse('POST', response, mr) as IResponse<PR>;
    }

    public async put({ vc, params, data, init, marshal = true }: APIRequestInit<TP, TD>): Promise<IResponse<TR>> {
        return this.request<TP, TD, TR>({ vc, method: 'PUT', data, params, init, marshal });
    }

    public marshalPutResponse(response: IResponse<any>, mr?: IModelsRegistry): IResponse<TR> {
        return this.marshalResponse('PUT', response, mr) as IResponse<TR>;
    }

    public async delete({ vc, params, data, init, marshal = true }: APIRequestInit<DP, DD>): Promise<IResponse<DR>> {
        return this.request<DP, DD, DR>({ vc, method: 'DELETE', data, params, init, marshal });
    }

    public marshalDeleteResponse(response: IResponse<any>, mr?: IModelsRegistry): IResponse<DR> {
        return this.marshalResponse('DELETE', response, mr) as IResponse<DR>;
    }

    private computeURLParts(urlPartParams?: string[], params?: IParams): IParams {
        const urlParts: IParams = {};
        if (urlPartParams === undefined || !Array.isArray(urlPartParams) || !urlPartParams.length) {
            return urlParts;
        }

        if (!params) {
            throw new APIClientError(` ${this.config.name} api requires params '${urlPartParams}' to construct url`);
        }
        urlPartParams.forEach((param) => {
            if (!Object.prototype.hasOwnProperty.call(params, param)) {
                throw new APIClientError(`${this.config.name} api requires "${param}" parameter to construct url`);
            }
            urlParts[param] = params[param];
        });
        return urlParts;
    }
}

export default {
    APIRequest,
    APIEndpoint,
};
