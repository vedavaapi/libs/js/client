/**
 * /objstore/v1 api
 * @see https://r1.bharatikosha.org/api/objstore/v1/docs
 */

import { IContext, IResource, getMarshalMiddleware } from '../../context';
import {
    APIEndpoint,
    IParams,
    urlMakerFnFactory,
} from '../lib';

import * as pms from './param-models';
import * as rms from './response-models';

export { pms, rms };

export const namespaceBase = '/objstore/v1';
const makeURLFn = urlMakerFnFactory(namespaceBase);

/**
 * get/post/delete resources;
 */
export const Resources = new APIEndpoint<
    pms.IResourcesGetParams, rms.ResourcesResponse<IResource>, IParams,
    pms.IResourcesPostData, rms.IResourcesPostResponse,
    any, any, any,
    IParams, pms.IResourcesDeleteData, rms.IResourcesDeleteResponse
>({
    name: 'objstore.Resources',
    url: makeURLFn('/resources'),
    methods: {
        get: {
            middleware: rms.ResourcesResponse.getMarshalMiddleware(),
        },
        post: {
            middleware: getMarshalMiddleware<any, rms.IResourcesPostResponse>(),
        },
        delete: {

        },
    },
});

/**
 * get Resource;
 * this endpoint serves as unique url of a resource;
 */
export const Resource = new APIEndpoint<
    pms.IResGetParams, IResource,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'objstore.Resource',
    urlPartParams: ['resource_id'],
    url: (vc: IContext, { resource_id }: { resource_id: string}): string => `${vc.base}${namespaceBase}/resources/${resource_id}`,
    methods: {
        get: {
            middleware: getMarshalMiddleware<IResource, IResource>(),
        },
    },
});

/**
 * get/post graphs
 */
export const Graph = new APIEndpoint<
    pms.IGraphGetParams, rms.GraphResponse,
    IParams, pms.IGraphPostData, rms.IGraphPostResponse,
    any, pms.IGraphGetParams, rms.GraphResponse,
    any, any, any
>({
    name: 'objstore.Graph',
    url: makeURLFn('/graph'),
    methods: {
        get: {
            middleware: rms.GraphResponse.getMarshalMiddleware(),
        },
        post: {
            middleware: getMarshalMiddleware<any, rms.IGraphPostResponse>(),
            odInfo: {
                files: {
                    multi: true,
                },
            },
        },
        put: {
            middleware: rms.GraphResponse.getMarshalMiddleware(),
        },
    },
});

/**
 * get/deltete referrers
 */
export const Referrers = new APIEndpoint<
    pms.IReferrersGetParams, rms.ResourcesResponse<IResource>,
    any, any, any,
    any, any, any,
    pms.IReferrersSelectorParams, pms.IReferrersDeleteData, rms.IResourcesDeleteResponse
>({
    name: 'objstore.Referrers',
    urlPartParams: ['resource_id', 'referrers_type'],
    url: (vc: IContext, { resource_id, referrers_type }: pms.IReferrersSelectorParams) => `${vc.base}${namespaceBase}/resources/${resource_id}/${referrers_type}`,
    methods: {
        get: {
            middleware: rms.ResourcesResponse.getMarshalMiddleware(),
        },
        delete: {

        },
    },
});

/** get schema of a jsonClass */
export const Schemas = new APIEndpoint<
    { json_class: string }, any,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'objstore.Schemas',
    urlPartParams: ['json_class'],
    url: (vc: IContext, { json_class }: { json_class: string}): string => `${vc.base}${namespaceBase}/schemas/${json_class}`,
    methods: {
        get: {
            middleware: getMarshalMiddleware<IResource, IResource>(),
        },
    },
});

/**
 * get opaque file, given it's id;
 * this endpoint serves as primary unique url for an oold resource's opaque file representation
 */
export const Files = new APIEndpoint<
    { file_id: string }, Blob,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'objstore.Files',
    urlPartParams: ['file_id'],
    url: (vc: IContext, { file_id }: { file_id: string}): string => `${vc.base}${namespaceBase}/files/${file_id}`,
    methods: {
        get: {

        },
    },
});
