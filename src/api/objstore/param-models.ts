import { IResource } from '../../context';
import { IIDResourceMap, IProjection, IResourcesGetCommonParams } from '../api-param-models';
import { IParams } from '../lib';

export interface IResourcesGetParams extends IResourcesGetCommonParams {
    /**
     * selector_doc; supports all mongo query syntax
     * @see https://docs.mongodb.com/manual/tutorial/query-documents/
     */
    selector_doc?: any;
}

export interface IResourcesPostData extends IParams {
    /** resources to be posted */
    resource_jsons: IResource[];
    return_projection?: IProjection;
    /** should upsert on primary_key match */
    upsert?: boolean;
}

export interface IResourcesDeleteData extends IParams {
    resource_ids: string[];
}


export interface IResGetParams extends IParams {
    resource_id: string;
    projection?: IProjection;
    attach_in_links?: boolean;
}


export interface IGraphGetParams extends IParams {
    start_nodes_selector: IParams;
    traverse_key_filter_maps_list?: Array<{ [field: string]: IParams }>;
    direction: 'referrer' | 'referred';
    max_hops?: number;
    hop_inclusions_config?: Array<boolean | string>;
    include_incomplete_paths?: boolean;
    include_ool_data_graph?: boolean;
    include_acls_graph?: boolean;
    json_class_projection_map?: { [jsonClass: string]: IProjection };
    attach_in_links?: boolean;
}

export interface IGraphPostData extends IParams {
    graph: IIDResourceMap;
    ool_data_graph?: IIDResourceMap;
    files?: any;
    upsert?: boolean;
    response_projection_map?: {
        [jsonClass: string]: IProjection;
    };
    should_return_resources?: boolean;
    should_return_oold_resources?: boolean;
}

export interface IReferrersSelectorParams {
    resource_id: string;
    referrers_type: 'specific_resources' | 'annotations';
}

export interface IReferrersGetParams extends IResourcesGetCommonParams, IReferrersSelectorParams {
    filter_doc?: any;
}

export interface IReferrersDeleteData extends IParams {
    filter_doc?: any;
}

export { IIDResourceMap, IParams, IProjection };
