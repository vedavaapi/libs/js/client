import { IResource, IModelsRegistry } from '../../context';


export interface IResourcesResponse {
    items: IResource[];
    count?: number;
    selector_doc?: any;
    total_count?: number;
}


/**
 * Resources response
 */
export class ResourcesResponse<RT extends IResource> implements IResourcesResponse {
    public static make<RT extends IResource>(response: IResourcesResponse, mr?: IModelsRegistry): ResourcesResponse<RT> | null {
        if (!Object.prototype.hasOwnProperty.call(response, 'items')) {
            return null;
        }
        if (mr !== undefined) {
            response.items.forEach((item) => {
                // @ts-ignore
                mr.make(item);
            });
        }
        Object.setPrototypeOf(response, ResourcesResponse.prototype);
        return response as ResourcesResponse<RT>;
    }

    public static getMarshalMiddleware<DT extends IResourcesResponse, MT extends IResource>() {
        return (data: DT, mr?: IModelsRegistry) => (ResourcesResponse.make<MT>(data, mr) as ResourcesResponse<MT>);
    }

    /** array of resources */
    public readonly items: IResource[] = [];

    /** count of resources returned */
    public readonly count?: number;

    /** queiried selector_doc */
    // tslint:disable-next-line: variable-name
    public readonly selector_doc?: any;

    /** total count of matched resources */
    // tslint:disable-next-line: variable-name
    public readonly total_count?: number;
}

/** post resources response. array of updated resources */
export interface IResourcesPostResponse {
    [index: number]: IResource;
}

/** resources delete response */
export interface IResourcesDeleteResponse {
    /** all dleteted ids, including deleted referres in entire referrer hierarchy, if any */
    all_deleted_ids: string[];
    /** delete success status for individual, exolicitly mentioned resources */
    delete_status: {
        [id: string]: boolean;
    };
}

/** @deprecated */
export interface IResourcesDeleteResponseOld {
    [index: number]: {
        deleted: boolean;
        deleted_resource_ids: string[];
    };
}


export interface IGraph {
    [_id: string]: IResource;
}


/** graph query response */
export interface IGraphResponse {
    /** id: doc map of all traversed&included resources. they normally have _reached_ids preserving traversal information */
    graph: IGraph;
    /** id: doc map off ool data referred in resources of graph */
    ool_data_graph?: IGraph;
    acls_graph?: {[id: string]: any};
    /** start node ids */
    start_nodes_ids: string[];
    /** order of resources inclusion */
    graph_order?: string[];
}

export class GraphResponse implements IGraphResponse {
    public static make(response: IGraphResponse, mr?: IModelsRegistry): GraphResponse | null {
        if (mr !== undefined) {
            Object.keys(response.graph).forEach((id) => {
                const res = response.graph[id];
                // @ts-ignore
                mr.make(res);
            });
        }
        Object.setPrototypeOf(response, GraphResponse.prototype);
        return response as GraphResponse;
    }

    public static getMarshalMiddleware() {
        return (data: IGraphResponse, mr?: IModelsRegistry) => GraphResponse.make(data, mr) as GraphResponse;
    }

    public graph: IGraph = {};

    public ool_data_graph?: IGraph;

    public acls_graph?: { [id: string]: any };

    public start_nodes_ids!: string[];

    public graph_order!: string[];

    // NOTE - should be trimmed down
    /** get resources reached from given resource in this graph traversal */
    public getReachedResources(resOrId: IResource | string, field: string): IResource[] {
        let res;
        if (typeof (resOrId) === 'object') {
            res = resOrId;
            if (!Object.prototype.hasOwnProperty.call(res, '_id')) {
                return [];
            }
        } else if (!Object.prototype.hasOwnProperty.call(this.graph, resOrId)) {
            return [];
        } else {
            res = this.graph[resOrId];
        }
        // eslint-disable-next-line no-underscore-dangle
        const reachedIds = res._reached_ids;
        if (reachedIds === undefined || !Object.prototype.hasOwnProperty.call(reachedIds, field)) {
            return [];
        }
        const rfIds = reachedIds[field];
        if (rfIds === undefined || !Array.isArray(rfIds)) {
            return [];
        }
        const reachedResources: IResource[] = [];
        rfIds.forEach((id) => {
            const reachedRes = this.graph[id];
            if (reachedRes === undefined) {
                return;
            }
            reachedResources.push(reachedRes);
        });
        return reachedResources;
    }
}

export interface IGraphPostResponse {
    graph: {
        [id: string]: IResource | string;
    };
    ool_data_graph: {
        [id: string]: IResource | string;
    };
}

export default {
    ResourcesResponse,
    GraphResponse,
};
