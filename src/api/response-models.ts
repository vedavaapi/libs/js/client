
/**
 * interface for any asynchronous task response.
 * gives status_url, and task_id. and normally Location header also will be setted with status_url
 */
export interface ITaskResponse {
    status_url: string;
    task_id: string;
}

/**
 * interface for any async task's status
 */
export interface ITaskStatus {
    state: 'PENDING' | 'PROGRESS' | 'SUCCESS' | 'FAILURE';
    status: string;
    current?: number;
    total?: number;
}

export interface ITaskDeleteStatus {
    success: boolean;
    task_id: string;
}
