/**
 * /importer/v1/iiif_importer namespace
 * @see https://r1.bharatikosha.org/api/tasker/v1/docs
 */

import { APIEndpoint, urlMakerFnFactory } from '../../lib';
import { ITasksGetParams, ITasksPostData, ITasksPutData, IStatsGetParams } from './param-models';
import { ITasksGetResponse, ITasksPostResponse, ITasksPutResponse, IStatsGetResult } from './response-models';

export { ITask } from './param-models';


export const namespaceBase = '/tasker/v1/manager';
const makeURLFn = urlMakerFnFactory(namespaceBase);


export const Tasks = new APIEndpoint<
    ITasksGetParams, ITasksGetResponse,
    any, ITasksPostData, ITasksPostResponse,
    any, ITasksPutData, ITasksPutResponse,
    any, any, any
>({
    name: 'Tasks',
    url: makeURLFn('/tasks'),
    methods: {
        get: {},
        post: {},
        put: {},
    },
});


export const Stats = new APIEndpoint<
    IStatsGetParams, IStatsGetResult,
    any, any, any,
    any, any, any,
    any, any, any
>({
    name: 'Stats',
    url: makeURLFn('/stats'),
    methods: {
        get: {},
    },
});
