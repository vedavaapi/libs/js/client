import { IParams } from '../../lib';
import { IProjection } from '../../api-param-models';


export type IState = 'PENDING' | 'PROGRESS' | 'PARTIAL' | 'SUCCESS' | 'FAILURE';
export type IResolvedState = 'PARTIAL' | 'SUCCESS' | 'FAILURE';

export interface ITaskPartial {
    jsonClass: string;
    _id?: string;
    name: string;
    label?: Array<{ chars: string, jsonClass: 'Text' }>,
    on: {
        target: {
            id: string;
            name?: string;
        },
        path?: Array<{
            meta_task_id?: string;
            target: { id: string; name?: string };
        }>
    },
    service: {
        name: string;
        id?: string;
    },
    actions: Array<'read' | 'updateContent' | 'updateLinks' | 'updatePermissions' | 'createAnnos' | 'createChildren' | 'delete'>;
    status: {
        id: string;
        url: string;
        state: IState;
    },
    work: {
        work_type: string;
        work_ext: string[];
        quantum?: string;
        quantity?: number;
        succeeded?: number;
        failed?: number;
    }
}


export interface ITask extends ITaskPartial {
    agent: {
        user: {
            id: string;
            name?: string;
        },
        teams?: Array<{
            id: string;
        }>
    },
    resolved: boolean;
    chronology: {
        started_at: string;
        updated_at: string;
        resolved_at?: string;
    }
}


export interface ITasksGetParams {
    selector_doc: IParams;
    projection?: IProjection;
    sort_doc?: Array<[string, 1 | -1]>;
    start?: number;
    count?: number;
    return_count_only?: boolean;
}


export interface ITasksPostData {
    tasks: Array<ITaskPartial>;
}


export interface ITasksPutData {
    state: IState;
    work_update?: {
        succeeded?: string;
        failed?: string;
    }
}


export interface IStatsGetParams {
    selector_docs: IParams[];
}
