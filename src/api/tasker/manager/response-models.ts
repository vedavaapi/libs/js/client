import { ITask } from './param-models';


export interface ITasksGetResponse {
    count: number;
    start: number;
    total_count: number;
    effective_count: number;
    items: ITask[];
}


export interface ITaskRegistrationInfo {
    task_id: string;
    task_token: string;
}


export type ITasksPostResponse = Array<{
    status: 'SUCCESS' | 'FAILURE'; // registration status
    info: ITaskRegistrationInfo;
}>;


export interface ITasksPutResponse {
    status: 'SUCCESS' | 'FAILURE';
}


export interface IStatsGetResult {
    count: number[];
}
