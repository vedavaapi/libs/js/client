
/**
 * Vedavaapi Resource Interface
 */
export interface IResource {
    jsonClass: string,
    metadata?: Array<{ jsonClass: 'MetadataItem', label: string, value: any }>;
    source?: string | string[];
    target?: string | string[];
    body?: any;
    selector?: any;
    jsonClassLabel?: string;
    _id?: string;
    [x: string]: any,
}

export interface IModelsRegistry {
    make(doc: IResource) : any;
    makeFrom(doc: any) : any;
}

export function getMarshalMiddleware<IT, OT>() {
    return (data: IT, mr?: IModelsRegistry) => {
        if (mr !== undefined) {
            return mr.makeFrom(data) as OT;
        }
        return data as unknown as OT;
    };
}


export interface IContext {
    readonly base: string;
    accessToken?: string;
    modelsRegistry?: IModelsRegistry;
}

export class Context implements IContext {
    public readonly base: string;

    public accessToken?: string;

    public modelsRegistry?: IModelsRegistry;

    constructor({ base, accessToken, modelsRegistry }: {base: string; accessToken?: string; modelsRegistry?: IModelsRegistry}) {
        this.base = base.replace(/\/+$/, '');
        this.accessToken = accessToken;
        this.modelsRegistry = modelsRegistry;
    }
}
