/**
 * error will be rised on invalid endpoint api invocations
 */
export class APIClientError extends Error {
    public name = 'APIClientError';

    constructor(...params: any[]) {
        super(...params);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, APIClientError);
        }
    }
}
