import * as vweb from '@vedavaapi/web';

/**
 * info about any opaque data params in formData
 */
export interface IOpaqueDataInfo {
    [param: string]: {
        multi: boolean;
    }
}

/**
 * creates FormData object from given data; stringifies ant non string params; handles files according to OpaqueDataInfo
 * @param data
 * @param opaqueDataInfo
 */
export function getFormData(data: { [param: string]: any }, opaqueDataInfo?: IOpaqueDataInfo) {
    // @ts-ignore
    const formData = new vweb.FormData() as any;
    Object.keys(data).forEach((k) => {
        let v = data[k];
        if (v === undefined) {
            return;
        }
        if (opaqueDataInfo && Object.prototype.hasOwnProperty.call(opaqueDataInfo, k)) {
            const oc = opaqueDataInfo[k];
            let items = [];
            if (oc && oc.multi) {
                items = Array.from(v);
            } else {
                items = [v];
            }
            items.forEach((item) => {
                formData.append(k, item as Blob);
            });
        } else if (typeof (v) !== 'string') {
            v = JSON.stringify(v);
            formData.append(k, v);
        } else { formData.append(k, v); }
    });
    return formData;
}

/**
 * returns URLSearchParams string computed from given params;
 * TODO support multiple value
 * @param params
 * @param init
 */
export function getSearchParams(params: { [param: string]: any }, init?: string) {
    const sParams = new vweb.URLSearchParams(init);
    Object.keys(params).forEach((k) => {
        let v = params[k];
        if (v === undefined) {
            return;
        }
        if (typeof (v) !== 'string') {
            v = JSON.stringify(v);
        }
        sParams.append(k, v);
    });
    return sParams.toString();
}

/**
 * returns authorized headers if accessToken present in Context;
 * @param vc
 * @param headers
 */
export function getHeaders(vc: { accessToken?: string }, headers?: { [header: string]: string }): { [header: string]: string } {
    const finalHeaders = headers === undefined ? {} : headers;
    if (vc !== undefined && vc.accessToken) {
        finalHeaders.Authorization = `Bearer ${vc.accessToken}`;
    }
    return finalHeaders;
}
